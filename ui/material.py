import bpy

def node_tree_selector_draw(layout, mat, output_types):
    try:
        layout.prop_search( mat.corona, "node_tree", bpy.data, "node_groups")
    except:
        return False

    node = find_node(mat, output_types)
    if not node:
        if mat.corona.node_tree == '':
            layout.operator('corona.add_material_nodetree', icon='NODETREE')

            layout.separator()
            layout.label("Chocofur.com sample materials", icon='NODETREE')
            row = layout.row()
            row.operator('corona.add_chocofur_plastic')
            row.operator('corona.add_chocofur_metal')
            row = layout.row()
            row.operator('corona.add_chocofur_glass')
            row.operator('corona.add_chocofur_trans')
            return False
    return True

def find_node(material, nodetypes):
    if not (material and material.corona and material.corona.node_tree):
        return None

    node_tree =  material.corona.node_tree

    if node_tree == '':
        return None

    ntree = bpy.data.node_groups[node_tree]

    for node in ntree.nodes:
        nt = getattr(node, "bl_idname", None)
        if nt in nodetypes:
            return node
    return None

#---------------------------------------
# Material preview UI
#---------------------------------------
class CoronaMaterialPreview( bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'

    bl_context = "material"
    bl_label = "Preview"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        return context.scene.render.engine in cls.COMPAT_ENGINES and context.object is not None and context.object.active_material is not None

    def draw( self, context):
        # self.layout.template_preview( context.material, show_buttons=False)
        layout = self.layout
        scene = context.scene
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        row = layout.row()
        row.template_preview( context.material, show_buttons=False)
        layout.prop( crn_mat, "preview_quality")
        # layout.operator_context = 'EXEC_REGION_PREVIEW'
        layout.operator("corona.big_preview")


#---------------------------------------
# Material settings UI
#---------------------------------------
class CMSMtlDiffuse ( bpy.types.Panel):
    bl_label = 'Diffuse'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type == 'coronamtl'


    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # Diffuse settings.
        # Kd
        # layout.label( "Diffuse:")
        box = layout.box()

        split = box.split( percentage = 0.7)
        col = split.column()
        col.prop( crn_mat, "diffuse_level", text = "Diffuse Level")

        split = split.split( percentage = 0.5)
        col = split.column()
        col.prop( crn_mat, "kd", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_kd", text = "T", toggle = True)
        if crn_mat.use_map_kd:
            # box.prop( crn_mat.map_kd, "show_settings")
            # if crn_mat.map_kd.show_settings:
            box.prop_search( crn_mat.map_kd,
                            "texture",
                            material,
                            "texture_slots",
                            text = "Texture")
            # box.template_image( crn_mat.map_kd,
            #                 "texture",
            #                 crn_mat.image_user)
            box.prop( crn_mat.map_kd, "intensity")
            row = box.row( align = True)
            row.prop( crn_mat.map_kd, "uOffset")
            row.prop( crn_mat.map_kd, "vOffset")
            row = box.row( align = True)
            row.prop( crn_mat.map_kd, "uScaling")
            row.prop( crn_mat.map_kd, "vScaling")

        # Translucency
        split = box.split( percentage = 0.85)
        col = split.column()

        col.prop( crn_mat, "translucency_level", text = "Translucency Level")
        if crn_mat.use_map_translucency_level:
            box.prop( crn_mat.map_translucency_level, "show_settings")
            if crn_mat.map_translucency_level.show_settings:
                box.prop_search( crn_mat.map_translucency_level,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_translucency_level, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency_level, "uOffset")
                row.prop( crn_mat.map_translucency_level, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency_level, "uScaling")
                row.prop( crn_mat.map_translucency_level, "vScaling")

        col = split.column()
        col.prop( crn_mat, "use_map_translucency_level", text = "T", toggle = True)

        split = box.split( percentage = 0.85)
        col = split.column()

        col.prop( crn_mat, "translucency", text = "")
        if crn_mat.use_map_translucency:
            box.prop( crn_mat.map_translucency, "show_settings")
            if crn_mat.map_translucency.show_settings:
                box.prop_search( crn_mat.map_translucency,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_translucency, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency, "uOffset")
                row.prop( crn_mat.map_translucency, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_translucency, "uScaling")
                row.prop( crn_mat.map_translucency, "vScaling")

        col = split.column()
        col.prop( crn_mat, "use_map_translucency", text = "T", toggle = True)

class CMSMtlReflection ( bpy.types.Panel):
    bl_label = 'Reflection'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type == 'coronamtl'

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        # Reflection settings.
        box = layout.box()
        # Ns
        split = box.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "ns", text = "Reflection Level")

        col = split.column()
        col.prop( crn_mat, "use_map_ns", text = "T", toggle = True)
        if crn_mat.use_map_ns:
            box.prop( crn_mat.map_ns, "show_settings")
            if crn_mat.map_ns.show_settings:
                box.prop_search( crn_mat.map_ns,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_ns, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_ns, "uOffset")
                row.prop( crn_mat.map_ns, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_ns, "uScaling")
                row.prop( crn_mat.map_ns, "vScaling")

        # Ks
        split = box.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "ks", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_ks", text = "T", toggle = True)
        if crn_mat.use_map_ks:
            box.prop( crn_mat.map_ks, "show_settings")
            if crn_mat.map_ks.show_settings:
                box.prop_search( crn_mat.map_ks,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_ks, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks, "uOffset")
                row.prop( crn_mat.map_ks, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_ks, "uScaling")
                row.prop( crn_mat.map_ks, "vScaling")

        # Reflect glossiness
        row = box.row( align = True)
        row.prop( crn_mat, "reflect_glossiness")
        # Reflect fresnel
        row.prop( crn_mat, "reflect_fresnel")
        # Anisotropy
        split = box.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "anisotropy", text = "Anisotropy")

        col = split.column()
        col.prop( crn_mat, "use_map_aniso", text = "T", toggle = True)
        if crn_mat.use_map_aniso:
            box.prop( crn_mat.map_aniso, "show_settings")
            if crn_mat.map_aniso.show_settings:
                box.prop_search( crn_mat.map_aniso,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_aniso, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso, "uOffset")
                row.prop( crn_mat.map_aniso, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso, "uScaling")
                row.prop( crn_mat.map_aniso, "vScaling")
        # Anisotropy rotation.
        split = box.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "aniso_rotation", text = "Anisotropy Rotation")

        col = split.column()
        col.prop( crn_mat, "use_map_aniso_rot", text = "T", toggle = True)
        if crn_mat.use_map_aniso_rot:
            box.prop( crn_mat.map_aniso_rot, "show_settings")
            if crn_mat.map_aniso_rot.show_settings:
                box.prop_search( crn_mat.map_aniso_rot,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_aniso_rot, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso_rot, "uOffset")
                row.prop( crn_mat.map_aniso_rot, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_aniso_rot, "uScaling")
                row.prop( crn_mat.map_aniso_rot, "vScaling")

class CMSMtlRefraction ( bpy.types.Panel):
    bl_label = 'Refraction'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type == 'coronamtl'

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # Refraction settings.
        box = layout.box()
        split = box.split( percentage = 0.7)
        col = split.column()
        col.prop( crn_mat, "refract_level", text = "Refraction Level")

        split = split.split( percentage = 0.5)
        col = split.column()
        col.prop( crn_mat, "refract", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_refract", text = "T", toggle = True)
        if crn_mat.use_map_refract:
            box.prop( crn_mat.map_refract, "show_settings")
            if crn_mat.map_refract.show_settings:
                box.prop_search( crn_mat.map_refract,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                box.prop( crn_mat.map_refract, "intensity")
                row = box.row( align = True)
                row.prop( crn_mat.map_refract, "uOffset")
                row.prop( crn_mat.map_refract, "vOffset")
                row = box.row( align = True)
                row.prop( crn_mat.map_refract, "uScaling")
                row.prop( crn_mat.map_refract, "vScaling")

        # Refraction Gloss / IOR
        row = box.row( align = True)
        row.prop( crn_mat, "refract_glossiness")
        row.prop( crn_mat, "ni")

        split = box.split()
        col = split.column()
        # col.active = crn_mat.refract_thin == False
        col.prop(  crn_mat, "refract_caustics")

        col = split.column()
        # col.active = crn_mat.refract_caustics == False
        col.prop( crn_mat, "refract_thin")


class CMSMtlVolume ( bpy.types.Panel):
    bl_label = 'Volumetric Absorption / Scattering'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type in ['coronamtl', 'coronavolumemtl']

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        layout.label( "Absorption:")

        split = layout.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "absorption_color", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_absorption", text = "T", toggle = True)
        if crn_mat.use_map_absorption:
            layout.prop( crn_mat.map_absorption, "show_settings")
            if crn_mat.map_absorption.show_settings:
                layout.prop_search( crn_mat.map_absorption,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                layout.prop( crn_mat.map_absorption, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_absorption, "uOffset")
                row.prop( crn_mat.map_absorption, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_absorption, "uScaling")
                row.prop( crn_mat.map_absorption, "vScaling")

        layout.prop( crn_mat, "absorption_distance", text = "Absorption Distance")

        layout.label( "Scattering:")
        split = layout.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "scattering_albedo", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_scattering", text = "T", toggle = True)
        if crn_mat.use_map_scattering:
            layout.prop( crn_mat.map_scattering, "show_settings")
            if crn_mat.map_scattering.show_settings:
                layout.prop_search( crn_mat.map_scattering,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                layout.prop( crn_mat.map_scattering, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_scattering, "uOffset")
                row.prop( crn_mat.map_scattering, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_scattering, "uScaling")
                row.prop( crn_mat.map_scattering, "vScaling")

        layout.prop( crn_mat, "mean_cosine")


class CMSMtlOpacity ( bpy.types.Panel):
    bl_label = 'Opacity'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type in ['coronamtl', 'coronalightmtl']

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        # Opacity
        layout.label( "Opacity:")
        split = layout.split( percentage = 0.85)
        col = split.column()
        col.prop( crn_mat, "opacity", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_opacity", text = "T", toggle = True)
        if crn_mat.use_map_opacity:
            layout.prop( crn_mat.map_opacity, "show_settings")
            if crn_mat.map_opacity.show_settings:
                layout.prop_search( crn_mat.map_opacity,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                layout.prop( crn_mat.map_opacity, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_opacity, "uOffset")
                row.prop( crn_mat.map_opacity, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_opacity, "uScaling")
                row.prop( crn_mat.map_opacity, "vScaling")


class CMSMtlBump ( bpy.types.Panel):
    bl_label = 'Bump'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type == 'coronamtl'

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        layout.prop( crn_mat, "use_map_bump", text = "Use Bump Texture", toggle = True)
        if crn_mat.use_map_bump:
           # layout.prop( crn_mat.map_bump, "show_settings")
           # if crn_mat.map_bump.show_settings:
            layout.prop_search( crn_mat.map_bump,
                           "texture",
                           material,
                           "texture_slots",
                           text = "Texture")
            layout.prop( crn_mat.map_bump, "intensity")
            row = layout.row( align = True)
            row.prop( crn_mat.map_bump, "uOffset")
            row.prop( crn_mat.map_bump, "vOffset")
            row = layout.row( align = True)
            row.prop( crn_mat.map_bump, "uScaling")
            row.prop( crn_mat.map_bump, "vScaling")


class CMSMtlEmission ( bpy.types.Panel):
    bl_label = 'Emission'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type in ['coronamtl', 'coronalightmtl', 'coronavolumemtl']

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        # Emission settings
        split = layout.split( percentage = 0.7)
        col = split.column()
        col.prop( crn_mat, "emission_mult", text = "Emission Level")

        split = split.split( percentage = 0.5)
        col = split.column()
        col.prop( crn_mat, "ke", text = "")

        col = split.column()
        col.prop( crn_mat, "use_map_ke", text = "T", toggle = True)
        if crn_mat.use_map_ke:
            layout.prop( crn_mat.map_ke, "show_settings")
            if crn_mat.map_ke.show_settings:
                layout.prop_search( crn_mat.map_ke,
                                "texture",
                                material,
                                "texture_slots",
                                text = "Texture")
                layout.prop( crn_mat.map_ke, "intensity")
                row = layout.row( align = True)
                row.prop( crn_mat.map_ke, "uOffset")
                row.prop( crn_mat.map_ke, "vOffset")
                row = layout.row( align = True)
                row.prop( crn_mat.map_ke, "uScaling")
                row.prop( crn_mat.map_ke, "vScaling")

        layout.separator()

        layout.prop( crn_mat, "emission_gloss", text = "Directionality")

class CMSMtlIES ( bpy.types.Panel):
    bl_label = 'IES'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona if material is not None else None
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None and crn_mat.node_tree == '' and crn_mat.mtl_type in ['coronalightmtl']

    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona
        # IES settings.
        layout.prop( crn_mat, "ies_profile")
        layout.prop( crn_mat, "keep_sharp", text = "Keep Profile Sharp")
        layout.prop( crn_mat, "ies_translate")
        layout.prop( crn_mat, "ies_rotate")
        layout.prop( crn_mat, "ies_scale")

#---------------------------------------
# Material Preset Menus
#---------------------------------------
class MATERIAL_MT_Corona_presets( bpy.types.Menu):
    bl_label = "Presets"
    preset_subdir = "corona/material"
    preset_operator = "script.execute_preset"
    draw = bpy.types.Menu.draw_preset
    
#---------------------------------------
# Material settings UI
#---------------------------------------
class CoronaMaterialShading( bpy.types.Panel):
    bl_label = 'Corona Surface Shading'
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA' and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None


    def draw( self, context):
        layout = self.layout
        obj = context.object
        material = obj.active_material
        crn_mat = material.corona

        #layout.prop( crn_mat, "preview_quality")

        layout.label( "Blender To Corona Material Conversion:")
        row = layout.row( align = True)
        row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert Material")
        row.operator( "corona.mat_convert", icon = 'MATERIAL', text = "Convert All").scene_wide = True
        
        layout.separator()
        row = layout.row( align = True)
        row.menu( "MATERIAL_MT_Corona_presets", text = MATERIAL_MT_Corona_presets.bl_label)
        row.operator( "corona.add_preset", text = "", icon = "ZOOMIN")
        row.operator( "corona.add_preset", text = "", icon = "ZOOMOUT").remove_active = True
        
        layout.separator()

        node_tree_selector_draw( layout, material, { 'CoronaMtlNode', 'CoronaLightMtlNode', 'CoronaVolumeMtlNode'})
        if crn_mat.node_tree != '':
            node_tree = bpy.data.node_groups[ crn_mat.node_tree]
            layout.prop_search( crn_mat, "node_output", node_tree, "nodes")

        if crn_mat.node_tree == '':
            layout.prop( crn_mat, "mtl_type")

            if crn_mat.mtl_type == 'coronaportalmtl':
                layout.label( "NOTE: Portal materials work differently in Corona")
                layout.label( "than in other renderers. You don't have to cover")
                layout.label( "all openings, and they work with scenes that have")
                layout.label( "both interior and exterior.")
                layout.label( "There is no poly count restriction, and there is")
                layout.label( "also a portal switch in regular CoronaMtl to turn")
                layout.label( " any material (e.g. glass window material) into a portal.")


            if crn_mat.mtl_type == 'coronamtl':
                layout.label( "Light Portal Material", icon = "OUTLINER_OB_LAMP")
                layout.prop( crn_mat, "as_portal")

        if crn_mat.mtl_type in ('coronalightmtl', 'coronamtl'):
            layout.label( "Lightmix Group", icon = "OUTLINER_OB_LAMP")
            layout.prop( crn_mat, "use_lightmix")
            if crn_mat.use_lightmix:
                layout.prop( crn_mat, "lightmix_group")

#---------------------------------------
# Ubershader operator UI
#---------------------------------------
class CoronaUbershaderPanel( bpy.types.Panel):
    bl_label = "Corona Material Nodes"
    bl_space_type = "NODE_EDITOR"
    bl_region_type = 'UI'

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer in ['CORONA', 'CYCLES'] and context.object is not None and context.object.type in ['MESH', 'SURFACE'] and context.object.active_material is not None

    def draw( self, context):
        layout = self.layout
        layout.operator( "corona.create_ubershader")


def register():
    bpy.utils.register_class( CoronaUbershaderPanel)
    bpy.utils.register_class( CoronaMaterialPreview)
    bpy.utils.register_class( MATERIAL_MT_Corona_presets)
    bpy.utils.register_class( CoronaMaterialShading)
    bpy.utils.register_class( CMSMtlDiffuse)
    bpy.utils.register_class( CMSMtlReflection)
    bpy.utils.register_class( CMSMtlRefraction)
    bpy.utils.register_class( CMSMtlVolume)
    bpy.utils.register_class( CMSMtlOpacity)
    bpy.utils.register_class( CMSMtlBump)
    bpy.utils.register_class( CMSMtlEmission)
    bpy.utils.register_class( CMSMtlIES)

def unregister():
    bpy.utils.unregister_class( CoronaUbershaderPanel)
    bpy.utils.unregister_class( CoronaMaterialShading)
    bpy.utils.unregister_class( MATERIAL_MT_Corona_presets)
    bpy.utils.unregister_class( CoronaMaterialPreview)
    bpy.utils.unregister_class( CMSMtlDiffuse)
    bpy.utils.unregister_class( CMSMtlReflection)
    bpy.utils.unregister_class( CMSMtlRefraction)
    bpy.utils.unregister_class( CMSMtlVolume)
    bpy.utils.unregister_class( CMSMtlOpacity)
    bpy.utils.unregister_class( CMSMtlBump)
    bpy.utils.unregister_class( CMSMtlEmission)
    bpy.utils.unregister_class( CMSMtlIES)



