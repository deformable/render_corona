import bpy
import bpy_extras.io_utils
import bmesh
import os, subprocess
import time
import struct
import math, mathutils
import io, zlib, concurrent.futures
from extensions_framework   import util as efutil
from .util                  import *

#---------------------------------------------
# General utilities and settings
#---------------------------------------------
def objWriter (verts, vn, vt, faces, filepath):
    file = open( filepath.decode('utf8'), "wb")
    fw = file.write
    # fp = fopen( filepath, "w");
    fw(verts)
    # fprintf( fp, verts);
    fw(vn)
    # fprintf( fp, vn);
    fw(vt)
    # fprintf( fp, vt);
    fw(faces)
    # fprintf( fp, faces);
    # fclose( fp);
    file.close()

def cpy_write( file, string):
    file.write( string)

def veckey3d(v):
    x = v.x
    y = v.y
    z = v.z
    return x, y, z

def calc_decrement( first, last, segments):
    return first * (( 1 - (last / first)) / segments)

def get_hairs( obj, scene, psys, crv_ob, crv_data, mat_name, emitter):


    for mod in obj.modifiers:
        if mod.type == 'PARTICLE_SYSTEM':
            if mod.particle_system.name == psys.name:
                break

    if not mod.type == 'PARTICLE_SYSTEM':
        return
    elif not mod.particle_system.name == psys.name or not mod.show_render:
        return

    debug('Exporting Hair system "%s"...' % psys.name)
    debug('mod', mod)

    root_size = psys.settings.corona.root_size
    tip_size = psys.settings.corona.tip_size
    # Set the render resolution of the particle system
    psys.set_resolution( scene, obj, 'RENDER')
    steps = 2 ** psys.settings.render_step + 1
    num_parents = len( psys.particles)
    num_children = len( psys.child_particles)
    curves = []
    crv_meshes = []
    c_append = curves.append
    cm_append = crv_meshes.append
    transform = obj.matrix_world.inverted()

    # colors = []
    # uv_coords = []
    uvflag = len(obj.data.uv_layers) > 0 and psys.settings.corona.export_color != 'none'
    colorflag = 0
    vertex_color_layer = None
    uv_tex = None
    image_width = 0
    image_height = 0
    image_pixels = []
    debug("Object", obj.data)
    obj.data.update(calc_tessface=True)
    # uv_layers = obj.data.uv_layers
    uv_textures = obj.data.tessface_uv_textures
    vertex_color = obj.data.tessface_vertex_colors
    has_vertex_colors = vertex_color.active and vertex_color.active.data
    # if psys.settings.corona.export_color == 'vertex_color':
    #     if has_vertex_colors:
    #         vertex_color_layer = vertex_color.active.data
    #         colorflag = 1

    # if uv_textures.active and uv_textures.active.data:
    #     uv_tex = uv_textures.active.data
    #     if psys.settings.corona.export_color == 'uv_texture_map':
    #         if uv_tex[0].image:
    #             image_width = uv_tex[0].image.size[0]
    #             image_height = uv_tex[0].image.size[1]
    #             image_pixels = uv_tex[0].image.pixels[:]
    #             colorflag = 1
    #     uvflag = 1

    debug("Setting", psys.settings.corona.export_color)
    debug("Colorflag", colorflag)
    debug("UVflag", uvflag)
    debug("HasVertexColors", has_vertex_colors)
    debug("UvTextures", uv_textures)

    if uvflag:
        debug("uv on emitter", psys.uv_on_emitter(mod, psys.particles[0]))

    # num_children is pre multiplied
    total = num_children if num_children != 0 else num_parents
    debug("Total particles", total, "parents", num_parents, "children", num_children, "particles", len(psys.particles))
    for p in range( 0, total):

        i = 0
        # if num_children != 0:
        #     i = math.floor(p / num_children)
        particle = None
        if p >= num_parents:
           particle = psys.particles[(p - num_parents) % num_parents]
        else:
           particle = psys.particles[p]


        crv = bpy.data.curves.new( 'hair_curve_%d' % p, 'CURVE')
        curves.append( crv)
        crv.splines.new( 'NURBS')
        points = crv.splines[0].points
        crv.splines[0].points.add( steps - 1)
        crv.splines[0].use_endpoint_u = True
        crv.splines[0].order_u = 4
        crv.dimensions = '3D'
        crv.fill_mode = 'FULL'
        if psys.settings.corona.shape == 'thick':
            crv.bevel_depth = psys.settings.corona.scaling
            crv.bevel_resolution = psys.settings.corona.resolution
        else:
            crv.extrude = psys.settings.corona.scaling
        crv.resolution_u = 1
        p_rad = root_size
        # rad_decrement = calc_decrement( root_size, tip_size, steps) * (psys.settings.corona.hair_shape + 1)
        shaft_size = min(root_size, tip_size)
        diff = root_size - tip_size
        co = None
        curvesteps = steps

        #      0   1   2   3   4
        # -1   0   0   0   0   0
        # -0.5 0   0.0 0.2 0.3 1.0
        #  0   0   0.2 0.4 0.6 0.8
        #  1   1.0 1.0 1.0 1.0 1.0



        # 5.   x     *
        # 4.  x    *
        # 3. x   *  y
        # 2.x  * y
        # 1.x*y
        # 0*
        #  0 1 2 3 4 5
        shapemod = (psys.settings.corona.hair_shape + 1) * (psys.settings.corona.hair_shape + 1)

        if tip_size > 0.0 and psys.settings.corona.close_tip:
            curvesteps -= 1

        step_size = diff / curvesteps

        for step in range(0, curvesteps):
            co = psys.co_hair( obj, p, step)
            points[step].co = mathutils.Vector( ( co.x, co.y, co.z, 1.0))
            points[step].radius = shaft_size + (curvesteps - step * shapemod) * step_size

            # p_rad
            # diff = ((tip_size - root_size) / curvesteps) * (step + 1) * (psys.settings.corona.hair_shape + 1)
            # p_rad = max(p_rad - diff, tip_size) if tip_size > root_size else min(p_rad - diff, tip_size)
            # Handle tip bigger than root
            # p_rad = max(p_rad - rad_decrement, tip_size) if tip_size > root_size else min(p_rad - rad_decrement, tip_size)

        if psys.settings.corona.close_tip:
            if tip_size == 0.0:
                points.add(1)
            points[curvesteps].co = mathutils.Vector( (co.x, co.y, co.z, 1.0))
            points[curvesteps].radius = 0.0

        crv.transform( transform)
        # Create an object for the curve, add the material, then convert to mesh
        crv_ob.data = bpy.data.curves[crv.name]
        if mat_name != 'Default':
            crv_ob.data.materials.append( bpy.data.materials[mat_name])
        mesh = crv_ob.to_mesh( scene, True, 'RENDER', calc_tessface = True)

        mesh_triangulate(mesh)

        uv_co = None
        col = None

        if uvflag:
            uv_co = psys.uv_on_emitter(mod, particle, p, uv_textures.active_index)
            # uv_coords.append(uv_co)

        # if psys.settings.corona.export_color == 'uv_texture_map' and len(image_pixels) != 0 and uv_co != None:
        #     x_co = round(uv_co[0] * (image_width - 1))
        #     y_co = round(uv_co[1] * (image_height - 1))

        #     pixelnumber = (image_width * y_co) + x_co

        #     r = image_pixels[pixelnumber * 4]
        #     g = image_pixels[pixelnumber * 4 + 1]
        #     b = image_pixels[pixelnumber * 4 + 2]
        #     col = (r, g, b)

        #     # colors.append(col)
        # elif psys.settings.corona.export_color == 'vertex_color' and has_vertex_colors:
        #     col = psys.mcol_on_emitter(mod, psys.particles[i], pindex, vertex_color.active_index)

            # colors.append(col)

        # debug("UV ", uv_co, "col", col)

        if uv_co != None:
            bm = bmesh.new()
            bm.from_mesh(mesh)

            uv_layer = None
            if len(bm.loops.layers.uv) == 0:
                uv_layer = bm.loops.layers.uv.new()
            # debug("Layers", len(bm.loops.layers.uv))
            uv_layer = bm.loops.layers.uv[0]
            bm.faces.ensure_lookup_table()
            nFaces = len(bm.faces)
            for fi in range(nFaces):
                faceloops = bm.faces[fi].loops
                for fl in range(len(faceloops)):
                    faceloops[fl][uv_layer].uv = uv_co

            bm.to_mesh(mesh)
            bm.free()
            del bm

        crv_meshes.append( mesh)
        crv_ob.data = crv_data
        bpy.data.curves.remove( crv)
    psys.set_resolution( scene, obj, 'PREVIEW')
    return crv_meshes

def mesh_triangulate(me):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.triangulate(bm, faces=bm.faces, quad_method = 2, ngon_method = 2)
    bm.to_mesh(me)
    bm.free()
    del bm

def decide_format( self, obj_path, obj, scene, curves):
    if scene.corona.binary_obj:
        return write_binary_obj( self, obj_path, obj, scene, curves)
    elif use_def_mb( obj, scene):
        return write_def_obj( self, obj_path, obj, scene, curves)
    else:
        return write_obj( self, obj_path, obj, scene, curves)

def name_compat(name):
    if name is None:
        return 'None'
    else:
        return name.replace(' ', '_').replace(":", "_")

def test_nurbs_compat(ob):
    if ob.type != 'CURVE':
        return False
    for nu in ob.data.splines:
        if nu.point_count_v == 1 and nu.type != 'BEZIER':  # not a surface and not bezier
            return True
    return False

#---------------------------------------------
#   Geometry export
#---------------------------------------------
def export(self, scene, curves):
    if scene.corona.export_path == '':
        CrnError("Corona export path is not set!  Please set the export path before exporting objects.")
        return
    #First, add all instanced geometry to a set so we only export once
    self._inst_obs = {(bpy.data.objects[ob.corona.instance_mesh]) for ob in scene.objects if (is_proxy(ob, scene) and not ob.corona.use_external)}
    self._inst_obs.update( get_all_duplis( scene))
    self._inst_obs.update( get_all_psysobs())

    self._exported_meshes = set()

    #Call the function and actually write everything now
    filepath = realpath(scene.corona.export_path)
    return _write(self, scene, filepath, curves)

#---------------------------------------------
def _write( self, scene, filepath, curves):
    # Exit edit mode before exporting, so current object states are exported properly.
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode='OBJECT')

    mesh_dir = os.path.join( filepath, "meshes")
    if not os.path.exists( mesh_dir):
        os.mkdir( mesh_dir)

    bin_obj = scene.corona.binary_obj
    obj_ext = '.cgeo' if bin_obj else '.obj'

    # for obj in scene.objects:
    #     _export_object(self, obj, scene, mesh_dir, obj_ext, curves)

    results = []

    bpy.context.window_manager.progress_begin(0, len(scene.objects))
    completed = 0

    with concurrent.futures.ThreadPoolExecutor(max_workers=thread_count) as executor:
        for obj in scene.objects:
            future = executor.submit(_export_object, self, obj, scene, mesh_dir, obj_ext, curves)
            result = future.result(timeout = None)
            completed += 1
            bpy.context.window_manager.progress_update(completed)
            if result != None:
                results.append(result)

    bpy.context.window_manager.progress_end()

    if len(results) != 0:
        return '\n'.join(results)

    return

def _export_object(self, obj, scene, mesh_dir, obj_ext, curves):
    if do_export( obj, scene):
        full_path = os.path.join( mesh_dir, name_compat(obj.data.name) ) + obj_ext
        if scene.corona.obj_export_mode == 'PARTIAL' and os.path.isfile( full_path):
            # Don't write the file if it exists
            return None
        elif scene.corona.obj_export_mode == 'SELECTED':
            # Only write the file if it the object is selected
            if obj.select == True:
                return decide_format( self, full_path, obj, scene, curves)
            else:
                return None
        else:
            # If writing all, or the mode is 'PARTIAL' and the file does not exist
            return decide_format( self, full_path, obj, scene, curves)


__MATINDEX__ = 1

#--------------------------------------
# .obj write function
#--------------------------------------
def write_obj(self, filepath, ob, scene, curves):
    """
    Basic write function. The context and options must be already set.
    This will write one .obj file per object
    """

    global __MATINDEX__

    __MATINDEX__ = 0
    # Initialize totals, these are updated each object
    totverts = totuvco = totno = 1

    # Write using Python if exporting hair, to keep memory usage from becoming a problem
    EXPORT_HAIR = scene.corona.export_hair and has_hairsys( ob)
    if EXPORT_HAIR:
        crv = curves[0]
        crv_ob = curves[1]

    face_vert_index = 1

    globalNormals = {}

    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    meshes = []
    ob_mat = ob.matrix_world

    # Don't export dupli parents, only dupli child.
    if ob.dupli_type != 'NONE':
        return

    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return

    for exported in self._exported_meshes:
        if ob.data.name == exported.name:
            # self._exported_obs.add(ob)
            debug("mesh already exported", ob.data)
            return;

    self._exported_meshes.add(ob.data)

    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add(ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len(ob.particle_systems) > 0 and not render_emitter(ob):
        export_mesh = False


    mesh = None

    try:
        mesh = ob.to_mesh(scene, True, 'RENDER', calc_tessface=False)
        # if ob.corona.triangulate:
        mesh_triangulate( mesh)
    except RuntimeError:
        mesh = None
    if mesh is None:
        return

    # Create a list of meshes.
    if export_mesh:
        meshes.append(mesh)

    CrnProgress("Writing .obj file ", (name_compat(ob.name) + ".obj"))

    # Add any hair meshes, if there are hair particle systems.
    if EXPORT_HAIR:


        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                    CrnProgress("Exporting hair system", psys.name)
                    mat_index = psys.settings.material - 1
                    material = 'Default'
                    if len(ob.material_slots) != 0:
                        material = ob.material_slots[mat_index].name
                    meshes.extend(get_hairs(ob, scene, psys, crv_ob, crv, material, mesh))
        # Open a file for writing hairs
        try:
            file = open( filepath, "w", encoding = "utf8")
        except:
            CrnProgress( "Cannot create file %s. Check directory permissions." % filepath)
            return

    # Try for some optimizations:
    # Append all vert / vertex texture coords / face text lines and write them all
    verts = []
    verts_n = []
    verts_t = []
    faces = []
    edge_list = []

    # Time the export.
    time1 = time.time()

    obmaterials = ob.material_slots[:]
    obmaterial_names = [m.name if m else None for m in obmaterials]

    for me in meshes:
        verts.clear()
        verts_n.clear()
        verts_t.clear()
        faces.clear()
        edge_list.clear()
        #Export UVs
        faceuv = len(me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]

        # Make sure there is something to write
        if not (len(face_index_pairs) + len(me.vertices)):
            # clean up
            bpy.data.meshes.remove(me)
            return  # dont bother with this mesh.

        # Default to exporting normals for Corona
        if face_index_pairs:
            me.calc_normals()
            # debug("loops before", len(me.loops))
            me.calc_normals_split()
            # debug("loops after", len(me.loops))
            (poly, grps) = me.calc_smooth_groups()
            # debug("Groups", grps)

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index,
                                    hash(uv_texture[a[1]].image),
                                    a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index,
                                    a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.

        # Vertices
        for v in me_verts:
            verts.append('v %.8f %.8f %.8f\n' % v.co[:])

        if EXPORT_HAIR:
            write_verts = ('').join(verts)
            cpy_write( file, write_verts)
            del write_verts
        # UV
        if faceuv:
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_face_mapping = [None] * len(face_index_pairs)

            uv_dict = {}  # could use a set() here
            for f, f_index in face_index_pairs:
                uv_ls = uv_face_mapping[f_index] = []
                append = uv_ls.append
                for uv_index, l_index in enumerate(f.loop_indices):
                    uv = uv_layer[l_index].uv

                    uvkey = uv[0], uv[1]
                    try:
                        uv_k = uv_dict[uvkey]
                    except:
                        uv_k = uv_dict[uvkey] = len(uv_dict)
                        verts_t.append('vt %.8f %.8f\n' % uv[:])
                    append(uv_k)

            uv_unique_count = len(uv_dict)

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping

        if not faceuv:
            f_image = None
            verts_t.append('vt 0 0 0\n')

        if EXPORT_HAIR:
            write_tex = ('').join(verts_t)
            cpy_write( file, write_tex)
            del write_tex

        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                vertices = f.vertices
                for v_idx in vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        verts_n.append('vn %.8f %.8f %.8f\n' % noKey)

            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    verts_n.append('vn %.8f %.8f %.8f\n' % noKey)

        if EXPORT_HAIR:
            write_normals = ('').join(verts_n)
            cpy_write( file, write_normals)
            del write_normals

        for f, f_index in face_index_pairs:
            f_smooth = f.use_smooth
            f_mat = min(f.material_index, len(materials) - 1)

            if faceuv:
                tface = uv_texture[f_index]
                f_image = tface.image

            # MAKE KEY
            if faceuv and f_image:  # Object is always true.
                key = material_names[f_mat], f_image.name
            else:
                key = material_names[f_mat], None  # No image, use None instead.

            # CHECK FOR CONTEXT SWITCH
            if key == contextMat:
                pass  # Context already switched, dont do anything
            else:
                if key[0] is None and key[1] is None:
                    # Write a null material, since we know the context has changed.
                    faces.append("usemtl 0\n")

                else:
                    mat_data = mtl_dict.get(key)
                    if not mat_data:

                        # First add to global dict so we can export to mtl
                        # Then write mtl

                        # Make a new names from the mat and image name,
                        # converting any spaces to underscores with name_compat.

                        # If none image dont bother adding it to the name
                        # Try to avoid as much as possible adding texname (or other things)
                        # to the mtl name (see [#32102])...
                        mtl_name = key[0] #"%s" % name_compat(key[0])
                        mtl_id = __MATINDEX__
                        __MATINDEX__ += 1
                        mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image, mtl_id
                        mtl_rev_dict[mtl_name] = key

                    # faces.append("usemtl %s\n" % str(mat_data[3]))

                    # debug("mtl", mat_data[0], (',').join(obmaterial_names), obmaterial_names.index(mat_data[0]))
                    # faces.append("usemtl %s\n" % str(mat_data[3]))
                    try:
                        faces.append("usemtl %d\n" % obmaterial_names.index(mat_data[0]))
                    except ValueError:
                        if scene.corona.material_override:
                            faces.append("usemtl 0")
                        else:
                            return 'Object "%s" is missing a material' % ob.name

            contextMat = key

            f_v = [(vi, me_verts[v_idx]) for vi, v_idx in enumerate(f.vertices)]

            # Write faces
            faces.append('f')

            if faceuv:
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        faces.append(" %d/%d/%d" %
                                   (v.index + totverts,
                                    totuvco + uv_face_mapping[f_index][vi],
                                    globalNormals[veckey3d(v.normal)],
                                    ))

                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        faces.append(" %d/%d/%d" %
                                   (v.index + totverts,
                                    totuvco + uv_face_mapping[f_index][vi],
                                    no,
                                    ))

                face_vert_index += len(f_v)

            else:
                # No UV's
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        faces.append(" %d/1/%d" % (
                                   v.index + totverts,
                                   globalNormals[veckey3d(v.normal)],
                                   ))
                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        faces.append(" %d/1/%d" % (v.index + totverts, no))

            faces.append('\n')
            #End of face_index_pairs 'for' loop

        if EXPORT_HAIR:
            write_faces = ('').join(faces)
            cpy_write( file, write_faces)
            del write_faces

        if not EXPORT_HAIR:
            # Write the vertices
            write_verts_p = ('').join(verts).encode('UTF-8')
            verts_p = write_verts_p

            # Write vertex normals
            write_normals_p = ('').join(verts_n).encode('UTF-8')
            normals_p = write_normals_p

            # Write tex coords
            write_tex_p = ('').join(verts_t).encode('UTF-8')
            tex_p = write_tex_p

            # Write faces
            write_faces_p = ('').join(faces).encode('UTF-8')
            faces_p = write_faces_p

            ### Write everything to the file, if not writing hair ###
            filepath_p = filepath.encode('UTF-8')
            path = filepath_p
            objWriter( verts_p, normals_p, tex_p, faces_p, path)

        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)

    # Close the file if writing hair.
    if EXPORT_HAIR:
        file.close()

    # Clear leftover data
    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()
    self._inst_obs.clear()

    CrnInfo( "OBJ Export time: %.4f" % (time.time() - time1))


########################################
# BINARY CGEO FILE WRITER - Write these values for declarations:
########################################
def write_binary_obj(self, filepath, ob, scene, curves):
    """
    Binary .CGEO file write function
    """

    global __MATINDEX__

    __MATINDEX__ = 0
    # Initialize totals, these are updated each object
    totverts = totuvco = totno = 1

    TAG_ANIMATION_SNAPSHOT_START = b'\x00\xEF\xCD\xA3'
    TAG_ANIMATION_SNAPSHOT_END =   b'\x11\xBA\xD3\xFE'
    TAG_ANIM_TABLE_START =         b'\xA1\xCC\xD0\xFA'
    TAG_ANIM_TABLE_END =           b'\x23\xAC\x80\xE5'
    TAG_BOUNDING_BOX =             b'\xB5\xA8\x89\x2F'
    TAG_MESH_DATA =                b'\xA8\x1E\xF7\x35'
    TAG_MESH_VERTICES_END =        b'\xCC\x42\xC3\x42'
    TAG_MESH_NORMALS_END =         b'\xB0\x06\x0B\xB0'
    TAG_MESH_MAP_COORDS_END =      b'\xCA\x04\xAC\xCA'
    TAG_POINTCLOUD =               b'\xEB\xEB\x87\x13'
    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    meshes = []
    ob_mat = ob.matrix_world
    globalNormals = {}

    EXPORT_HAIR = scene.corona.export_hair and has_hairsys( ob)
    if EXPORT_HAIR:
        crv = curves[0]
        crv_ob = curves[1]

    # Don't export dupli parents, only dupli child.
    if ob.dupli_type != 'NONE':
        return

    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return

    for exported in self._exported_meshes:
        if ob.data.name == exported.name:
            # self._exported_obs.add(ob)
            debug("mesh already exported", ob.data.name)
            return;

    self._exported_meshes.add(ob.data)

    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add( ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len( ob.particle_systems) > 0 and not render_emitter( ob):
        export_mesh = False

    mesh = None
    try:
        mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface=False)
        # if ob.corona.triangulate:
        mesh_triangulate( mesh)
    except RuntimeError:
        mesh = None
    if mesh is None:
        return

    # Create a list of meshes.
    if export_mesh:
        meshes.append(mesh)


    CrnProgress( "Writing .cgeo file ", (name_compat(ob.data.name) + ".cgeo"))

    # Time the export.
    time1 = time.time()

    obmaterials = ob.material_slots[:]
    obmaterial_names = [m.name if m else None for m in obmaterials]

    if EXPORT_HAIR:
        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                    CrnProgress( "Exporting hair system", psys.name)
                    mat_index = psys.settings.material - 1
                    material = 'Default'
                    if len(ob.material_slots) != 0:
                        material = ob.material_slots[mat_index].name
                    meshes.extend( get_hairs(ob, scene, psys, crv_ob, crv, material, mesh))

    #Open the file for writing
    file = open( filepath, "wb")
    fw = file.write

    use_compress = False

    #Construct the header
    header = b'Version: 5\n'
    header += b'Animation snapshot count: 1\n'
    header += b'Time per animation snapshot [microseconds]: 0\n'
    header += b'Animation time start: 0\n'
    header += b'Scale: 1\n'
    header += b'Original material name: %s\n' % obmaterial_names[0].encode('utf8') if obmaterial_names else b'Material'
    header += b'Original object name: %s\n' % ob.data.name.encode('utf8')
    header += b'Compressed: ' + (b'True' if use_compress else b'False')

    pack = struct.pack

    vertbuf = io.BytesIO()
    vertfw = vertbuf.write
    normbuf = io.BytesIO()
    normfw = normbuf.write
    uvsbuf = io.BytesIO()
    uvsfw = uvsbuf.write
    trisbuf = io.BytesIO()
    trisfw = trisbuf.write

    combinedverts = 0
    combinednormals = 0
    # combineduvs = 0
    combinedtris = 0

    sections = []
    uvlayer_buff = {}

    for me in meshes:

        #Export UVs
        faceuv = len( me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            #uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]

        if not (len(face_index_pairs) + len(me.vertices)):  # Make sure there is somthing to write

            # clean up
            bpy.data.meshes.remove(me)

            return  # dont bother with this mesh.

        #Default to exporting normals for Corona, so test for face_index_pairs only
        #if face_index_pairs:
        me.calc_normals()

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, hash(uv_texture[a[1]].image), a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.

        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                for v_idx in f.vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        #Write vertex normals
                        normfw(pack('<fff', noKey[0], noKey[1], noKey[2]))

            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    normfw(pack('<fff', noKey[0], noKey[1], noKey[2]))

        # Write counts
        combinedverts += len(me_verts)
        combinednormals += totno-1
        # combineduvs += 1 if faceuv else 0
        combinedtris += len(face_index_pairs)

        totno = 1

        # Write verts
        for v in me_verts:
            vertfw(pack('<fff', v.co[0], v.co[1], v.co[2]))

        # UV
        uv_unique_count = 0
        if faceuv:
            # debug('uv layers', len( me.uv_textures))
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_layer_face_maps = [None] * len(me.uv_layers)


            layercount = 0
            for uvlayer in me.uv_layers:
                # debug("Starting uv layer", layercount, uvlayer.name)
                try:
                    bytesbuf = uvlayer_buff[uvlayer.name]['bytesbuf']
                    uv_dict = uvlayer_buff[uvlayer.name]['uv_dict']
                except:
                    uvlayer_buff[uvlayer.name] = {}
                    bytesbuf = uvlayer_buff[uvlayer.name]['bytesbuf'] = io.BytesIO()
                    uvlayer_buff[uvlayer.name]['uv_counter'] = 0
                    uv_dict = uvlayer_buff[uvlayer.name]['uv_dict'] = {}
                # uv_dict = {}  # could use a set() here
                uv_layer_face_maps[layercount] = uv_face_mapping = [None] * len(face_index_pairs)
                # bytesbuf = io.BytesIO()
                uv_counter = 0
                for f, f_index in face_index_pairs:
                    uv_ls = uv_face_mapping[f_index] = []
                    for uv_index, l_index in enumerate(f.loop_indices):
                        uv = uvlayer.data[l_index].uv
                        uvkey = uv[0], uv[1]

                        try:
                            uv_k = uv_dict[uvkey]
                        except:
                            uv_k = uv_dict[uvkey] = len(uv_dict)
                            uv_counter += 1
                            bytesbuf.write(pack('<fff', uv[0], uv[1], 0))
                        uv_ls.append(uv_k)

                uvlayer_buff[uvlayer.name]['uv_counter'] += uv_counter
                # debug("primary uv ", uv_counter)
                layercount += 1;

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping


        else:
            f_image = None
            uv_unique_count = 0
            # uvsfw(pack('<I', 1))
            # uvsfw(pack('<fff', 0, 0, 0))

        for f, f_index in face_index_pairs:
            f_smooth = f.use_smooth
            f_mat = min(f.material_index, len(materials) - 1)

            if faceuv:
                tface = uv_texture[f_index]
                f_image = tface.image

            # MAKE KEY
            if faceuv and f_image:  # Object is always true.
                key = material_names[f_mat], f_image.name
            else:
                key = material_names[f_mat], None  # No image, use None instead.

            matid = 0

            mat_data = mtl_dict.get(key)
            if not mat_data:

                # First add to global dict so we can export to mtl
                # Then write mtl

                # Make a new names from the mat and image name,
                # converting any spaces to underscores with name_compat.

                # If none image dont bother adding it to the name
                # Try to avoid as much as possible adding texname (or other things)
                # to the mtl name (see [#32102])...
                mtl_name = key[0] #"%s" % name_compat(key[0])
                mtl_id = __MATINDEX__
                __MATINDEX__ += 1
                mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image, mtl_id
                mtl_rev_dict[mtl_name] = key

            try:
                matid = obmaterial_names.index(mat_data[0])
            except ValueError as e:
                if scene.corona.material_override:
                    matid = 0
                else:
                    return 'Object "%s" is missing a material' % ob.name

            f_v = [(vi, me_verts[v_idx]) for vi, v_idx in enumerate(f.vertices)]

            # Write faces
            if faceuv:
                ccvx = []
                ccvt = []
                ccvn = []
                # Need to index faces from 0, not from 1
                # So subtract 1 from each value before writing
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        # ccvt.append(totuvco + uv_face_mapping[f_index][vi] - 1)
                        ccvn.append(globalNormals[veckey3d(v.normal)] - 1)

                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        # ccvt.append(totuvco + uv_face_mapping[f_index][vi] - 1)
                        ccvn.append(no - 1)

                for uv_face_mapping in uv_layer_face_maps:
                    for vi, v in f_v:
                        ccvt.append(totuvco + uv_face_mapping[f_index][vi] - 1)
                # Tri
                # Vertex Segment Count - 1
                trisfw(pack('<I', 0))
                # Vertex Indexes
                trisfw(pack('<III', ccvx[0], ccvx[1], ccvx[2]))
                # Normal Segment Count - 1
                trisfw(pack('<I', 0))
                trisfw(pack('<III', ccvn[0], ccvn[1], ccvn[2]))
                # Map Coordinates Indicies
                # for mapchannel in range(0, combineduvs):
                #     trisfw(pack('<III', ccvt[0], ccvt[1], ccvt[2]))
                for vt in ccvt:
                    trisfw(pack('<I', vt))
                # Material ID
                trisfw(pack('<H', matid))
                # Edge visibility
                trisfw(pack('<c', b'\x07'))

            else:
                ccvx = []
                ccvn = []
                # No UV's
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        ccvn.append(globalNormals[veckey3d(v.normal)] - 1)
                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        ccvn.append(no - 1)
                # Tri
                # Vertex Segment Count - 1
                trisfw(pack('<I', 0))
                # Vertex Indexes
                trisfw(pack('<III', ccvx[0], ccvx[1], ccvx[2]))
                # Normal Segment Count - 1
                trisfw(pack('<I', 0))
                trisfw(pack('<III', ccvn[0], ccvn[1], ccvn[2]))
                # Map Coordinates Indicies hard coded 1 map channels
                trisfw(pack('<III', 0, 0, 0))
                # Material ID
                trisfw(pack('<H', matid))
                # Edge visibility
                trisfw(pack('<c', b'\x07'))

        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)

    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()

    combineduvs = 0
    if faceuv:
        for key, container in uvlayer_buff.items():
            debug("Writing UV", key, container['uv_counter'])
            if container['uv_counter'] > 0:
                uvsfw(pack('<I', container['uv_counter']))
                uvsfw(container['bytesbuf'].getvalue())
                combineduvs += 1
            container['bytesbuf'].close();
    else:
        debug("Writing dummy UV")
        uvsfw(pack('<I', 1))
        uvsfw(pack('<fff', 0, 0, 0))
        combineduvs += 1


    sectionbuf = io.BytesIO()
    sectionfw = sectionbuf.write


    debug("Vertex Count", combinedverts)
    debug("Normals Count", combinednormals)
    debug("Mapping Channels Count", combineduvs)
    debug("Triangle Count", combinedtris)

    #snapshot flag
    sectionfw(pack('<B', 1))
    sectionfw(pack('<IIII',
        combinedverts, # Verts
        combinednormals,  # Normals
        combineduvs, # UV Mapping channels
        combinedtris # num triangles
        ))
    sectionfw(vertbuf.getvalue())
    vertbuf.close()
    sectionfw(TAG_MESH_VERTICES_END)

    sectionfw(normbuf.getvalue())
    normbuf.close()
    sectionfw(TAG_MESH_NORMALS_END)

    # if combineduvs > 0:
    sectionfw(uvsbuf.getvalue())
    uvsbuf.close()
    sectionfw(TAG_MESH_MAP_COORDS_END)

    sectionfw(trisbuf.getvalue())
    trisbuf.close()

    data = None
    if use_compress:
        data = zlib.compress(sectionbuf.getvalue(), 1)
    else:
        data = sectionbuf.getvalue()
    sectionbuf.close()

    sections.append(data)

    #Write the header
    fw(pack('<cI', b'\x43', len(header)))    #1 + 4
    fw(header)

    # Write flags
    fw(pack('<I', 1))               #4

    # Write the anim table
    fw(TAG_ANIM_TABLE_START)        #4

    # For each entry
    # 21 bytes of magic 1, header size 4, flags 4, TAG_ANIM_TABLE_START 4, TAG_ANIM_TABLE_END 4, TAG_ANIMATION_SNAPSHOT_START 4
    # 8 bytes of offset for each section
    pos = len(header) + 21 + (len(sections) * 8)
    for section in sections:
        fw(pack('<q', pos))
        # 12 bytes for TAG_MESH_DATA and section size
        pos += len(section) + 12

    fw(TAG_ANIM_TABLE_END)          #4
    fw(TAG_ANIMATION_SNAPSHOT_START)#4

    for section in sections:
        fw(TAG_MESH_DATA)           #4
        fw(pack('<Q', len(section)))#8
        fw(section)

    fw(TAG_ANIMATION_SNAPSHOT_END)

    file.close()

    self._inst_obs.clear()

    CrnInfo( "CGEO Export time: %.4f" % (time.time() - time1))

#--------------------------------------
# Deformation MB .obj write function
#--------------------------------------
def write_def_obj(self, filepath, ob, scene, curves):
    """
    Write function for deformed motion-blurred object.
    """

    global __MATINDEX__

    __MATINDEX__ = 0
    # Initialize totals, these are updated each object
    totverts = totuvco = totno = face_vert_index = 1
    meshes = []
    globalNormals = {}

    # Each value of startMeshVerts will be a list [v, vt, vn]
    #   for a face vertex at index of mesh_idx in start_mesh.
    # The key is mesh_idx.
    startMeshVerts = {}

    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    # Don't export dupli parents, only dupli child.
    if ob.dupli_type != 'NONE':
        return

    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return

    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add(ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len(ob.particle_systems) > 0 and not render_emitter(ob):
        export_mesh = False

    if export_mesh:
        try:
            frame_orig = scene.frame_current
            frame_set = scene.frame_set
            offset = 0.5 + ( scene.corona.frame_offset / 2)
            interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
            start = frame_orig - ( interval * ( 1 - offset))
            end = frame_orig + ( interval * offset)

            # Have to set subframe separately.
            frame_set( int(start), subframe = ( start % 1))
            start_mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface = True)

            frame_set( int( end), subframe = ( end % 1))
            end_mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface= True)

            # Triangulate for deformed objects
            mesh_triangulate( start_mesh)
            mesh_triangulate( end_mesh)

            # Reset timeline and return.
            frame_set( frame_orig)

        except RuntimeError:
            start_mesh = None
        if start_mesh is None:
            return

    # Create a list of meshes.
    meshes.extend( [ start_mesh, end_mesh])

    CrnProgress( "Writing .obj file ", ( name_compat(ob.name) + ".obj"))

    # Try for some optimizations:
    # Append all vert / vertex texture coords / face text lines and write them all
    verts = []
    verts_n = []
    verts_t = []
    faces = []
    edge_list = []

    # Time the export.
    time1 = time.time()

    obmaterials = ob.material_slots[:]
    obmaterial_names = [m.name if m else None for m in obmaterials]

    for me in meshes:
        mesh_idx = 1    # Reset mesh index

        #Export UVs
        faceuv = len(me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [ ( face, index) for index, face in enumerate( me.polygons)]

        # Make sure there is something to write
        if not (len(face_index_pairs) + len(me.vertices)):
            # clean up
            bpy.data.meshes.remove(me)
            return  # dont bother with this mesh.

        # Default to exporting normals for Corona
        if face_index_pairs:
            me.calc_normals()

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index,
                                    hash(uv_texture[a[1]].image),
                                    a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index,
                                    a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.

        # Vertices
        for v in me_verts:
            verts.append('v %.6f %.6f %.6f\n' % v.co[:])

        # UV
        if faceuv:
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_face_mapping = [None] * len(face_index_pairs)

            uv_dict = {}  # could use a set() here
            for f, f_index in face_index_pairs:
                uv_ls = uv_face_mapping[f_index] = []
                append = uv_ls.append
                for uv_index, l_index in enumerate(f.loop_indices):
                    uv = uv_layer[l_index].uv

                    uvkey = uv[0], uv[1]
                    try:
                        uv_k = uv_dict[uvkey]
                    except:
                        uv_k = uv_dict[uvkey] = len(uv_dict)
                        verts_t.append('vt %.6f %.6f\n' % uv[:])
                    append(uv_k)

            uv_unique_count = len(uv_dict)

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping

        if not faceuv:
            f_image = None
            verts_t.append('vt 0 0 0\n')

        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                vertices = f.vertices
                for v_idx in vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        verts_n.append('vn %.6f %.6f %.6f\n' % noKey)

            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    verts_n.append('vn %.6f %.6f %.6f\n' % noKey)

        # Faces.
        # Store face data on START mesh
        # Only add face data to faces list on the END mesh, looking up corresponding vert indices
        #   from start_mesh
        if me == start_mesh:
            for f, f_index in face_index_pairs:
                f_smooth = f.use_smooth
                f_mat = min(f.material_index, len(materials) - 1)

                if faceuv:
                    tface = uv_texture[f_index]
                    f_image = tface.image

                # MAKE KEY
                if faceuv and f_image:  # Object is always true.
                    key = material_names[f_mat], f_image.name
                else:
                    key = material_names[f_mat], None  # No image, use None instead.

                # CHECK FOR CONTEXT SWITCH
                if key == contextMat:
                    pass  # Context already switched, dont do anything

                contextMat = key

                f_v = [ ( vi, me_verts[ v_idx]) for vi, v_idx in enumerate( f.vertices)]

                if faceuv:
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                        totuvco + uv_face_mapping[ f_index][ vi],
                                        globalNormals[ veckey3d( v.normal)] ]

                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                        totuvco + uv_face_mapping[ f_index][ vi],
                                        globalNormals[ veckey3d( f.normal)] ]

                            mesh_idx += 1

                    face_vert_index += len(f_v)

                else:
                    # No UV's
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                       1,
                                       globalNormals[ veckey3d( v.normal)] ]

                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                       1,
                                       globalNormals[ veckey3d( f.normal)] ]

                            mesh_idx += 1

                #End of face_index_pairs 'for' loop

        else:
            # Iterating over end_mesh
            # Look up the corresponding vertices from start_mesh
            #   and append them to faces list
            for f, f_index in face_index_pairs:
                f_smooth = f.use_smooth
                f_mat = min(f.material_index, len(ob.materials) - 1)

                if faceuv:
                    tface = uv_texture[f_index]
                    f_image = tface.image

                # MAKE KEY
                if faceuv and f_image:  # Object is always true.
                    key = material_names[f_mat], f_image.name
                else:
                    key = material_names[f_mat], None  # No image, use None instead.

                # CHECK FOR CONTEXT SWITCH
                if key == contextMat:
                    pass  # Context already switched, dont do anything
                else:
                    if key[0] is None and key[1] is None:
                        # Write a null material, since we know the context has changed.
                        faces.append("usemtl 0\n")

                    else:
                        mat_data = mtl_dict.get(key)
                        if not mat_data:

                            # First add to global dict so we can export to mtl
                            # Then write mtl

                            # Make a new names from the mat and image name,
                            # converting any spaces to underscores with name_compat.

                            # If none image dont bother adding it to the name
                            # Try to avoid as much as possible adding texname (or other things)
                            # to the mtl name (see [#32102])...
                            mtl_name = key[0] #"%s" % name_compat(key[0])
                            mtl_id = __MATINDEX__
                            __MATINDEX__ += 1
                            mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image, mtl_id
                            mtl_rev_dict[mtl_name] = key

                        # faces.append("usemtl %s\n" % str(mat_data[3]))
                        faces.append("usemtl %d\n" % obmaterial_names.index(mtl_name))

                contextMat = key

                f_v = [ ( vi, me_verts[ v_idx]) for vi, v_idx in enumerate( f.vertices)]

                # Write faces
                faces.append('a 1')

                if faceuv:
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( v.normal)]
                                        ))

                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( f.normal)]
                                        ))

                            mesh_idx += 1

                    face_vert_index += len(f_v)

                else:
                    # No UV's
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( v.normal)]
                                        ))

                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        no = globalNormals[ veckey3d( f.normal)]
                        no_end = globalNormals[ veckey3d( f_end.normal)]
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( f.normal)]
                                        ))

                            mesh_idx += 1

                faces.append('\n')
                #End of face_index_pairs 'for' loop

        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)

    # Write the vertices
    write_verts_p = ('').join(verts).encode('UTF-8')
    verts_p = write_verts_p

    # Write vertex normals
    write_normals_p = ('').join(verts_n).encode('UTF-8')
    normals_p = write_normals_p


    # Write tex coords
    write_tex_p = ('').join(verts_t).encode('UTF-8')
    tex_p = write_tex_p

    # Write faces
    write_faces_p = ('').join(faces).encode('UTF-8')
    faces_p = write_faces_p

    ### Write everything to the file, if not writing hair ###
    filepath_p = filepath.encode('UTF-8')
    path = filepath_p
    objWriter( verts_p, normals_p, tex_p, faces_p, path)

    # Clear leftover data
    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()
    self._inst_obs.clear()

    CrnInfo( "OBJ Export time: %.4f" % (time.time() - time1))
